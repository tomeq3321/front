import React, { Component } from 'react';
import ParticlesBg from 'particles-bg';
import RegistrationForm from './RegistrationForm';

export default class Register extends Component {
    render() {
        return (
            <>
            <RegistrationForm/>
            <ParticlesBg color="#461D77" num={200} type="cobweb" bg={true} />
            </>
        )
    }
}