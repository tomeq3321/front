import React, { Component } from 'react';
import NavbarPage from './NavbarPage';
import auth from '../../services/authSevice';

function ShowNavbar() {
    if (auth.IsAuthenticated()) {
      return <NavbarPage />;
    }
    return null;
  }
  export default class ShowNavbarPage extends Component {
    render() {
        return (
            <ShowNavbar/>
        );
    }
}