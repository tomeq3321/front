import React, { Component } from 'react';
import ParticlesBg from 'particles-bg';
import HomePage from './HomePage';

export default class Home extends Component {
    render() {
        return (
            <>
            <HomePage/>
            <ParticlesBg color="#461D77" num={200} type="cobweb" bg={true} />
            </>
        )
    }
}