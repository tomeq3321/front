import React, { Component } from 'react';
import ParticlesBg from 'particles-bg';
import './NotFound.css';

export default class NotFound extends Component {
    render() {
        return (
            <>
            <div className="NotFound">
               <h1>Not found</h1>
            </div> 
            <ParticlesBg color="#461D77" num={200} type="cobweb" bg={true} />
            </>
        );
    }
}