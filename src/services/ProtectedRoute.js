import React from "react";
import {Route, Redirect} from "react-router-dom";
import auth from "./authSevice";

export const ProtectedRoute = ({ component: Component, ...rest }) => {
    return (
        <Route
        {...rest}
        render={props => {
            if (auth.IsAuthenticated()) {
                console.log("tak");
                return <Component {...props} />;
            } 
            else {
                console.log("nie")
                return (
                    
                <Redirect 
                    to={{
                        pathname: "/",
                        state: {
                            from: props.location
                        }
                    }} 
                />
                );
            }
        }}
        />
    );
};