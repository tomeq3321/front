import React from 'react'
import { Redirect } from 'react-router-dom'
import axios from 'axios'; 

class Auth {
    state = {
        redirect: false
      }
    path = "http://127.0.0.1:8000/api/"; 

    Login(username, password) {
        axios.post(this.path + "login_check", {username,password})
        .then(res => localStorage.setItem("token", res.data.token))
        .catch(err => { 
            console.log(localStorage.getItem("token"));
          });
        
          console.log('token'+localStorage.getItem("token"));
    }

    Register(bodyFormData)
    {
        axios({
            method: 'post',
            url: this.path + "register",
            data: bodyFormData,
            headers: {'Content-Type': 'multipart/form-data' }
        })
        .then(function (response) {
            //handle success
            console.log(response);
        })
        .catch(function (response) {
            //handle error
            console.log(response);
        });
    }

    Logout() {
        localStorage.removeItem("token");
    }

    IsAuthenticated()
    {
        if (localStorage.getItem("token") === null) {
            console.log('nies');
            return false;
        }
        else 
        {
            return true;
        }
    }
    ReturnToken() {
        console.log('tokennnn'+ localStorage.getItem("token"));
            return localStorage.getItem("token");
    }
}
export default new Auth();